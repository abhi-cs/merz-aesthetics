<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'merzaesthetics_wp');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'u.AL>U}=wQ_C.7;TskK1|YE7<33zmJ9$1)>l;-hX@.5+3k. %=]0N#ig1{pBT-Dr');
define('SECURE_AUTH_KEY',  '[n7q$Vua/ :9aqy[gq_fRhGT4G,T|4dW)h3yc1R=::1m(8jqeD67LgvPVIQNRW+A');
define('LOGGED_IN_KEY',    '&>u`aueLTE,G+%,72RxX14Z5bZ%lQ?c xYyz%o>x1S XN+E}eVS-xtl!3n(4[}|s');
define('NONCE_KEY',        ':scgf/K)G%>*Nu_)NRV]Mbp-n.P^0sW^kr+-#=vgr4zWSZXq$([8i(I9yv[H#NPq');
define('AUTH_SALT',        'tw)d~xO-w/Gbz_J8,M/VG$e(~Vh@ H1re`f6hcED}]7yLHB!~O7j{o0]9IAvd,6a');
define('SECURE_AUTH_SALT', '$^)4Hihe/}D|7}sk?&QCiryr.9n -r4Bo0{,kon}* Cr(#BS7j!sC<95Uc%=(3]I');
define('LOGGED_IN_SALT',   'uG<RN/DYpPUCw@:ArVwaBN~Lvr1p^:]N^mFg?d`j3kxo!HG5/9YAL6jhCO*|;tV;');
define('NONCE_SALT',       'i:baENRaR5UgmY)BC[ II~5j7DXS;%1v*JJr<2^du(/.Px$hgrF0aU(_kx~.n**Z');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
