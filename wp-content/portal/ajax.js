function Ajax(){
this.Method="GET";//String "GET" or "POST"
this.URL=null;
this.ResponseHandler=null;
this.ErrorHandler=null;
this.Request=null;
this.Async=true;
this.Data = null;
this.ResponseFormat="text"; 
this.ObjectState=null;
this.init=function(){
	if (!this.Request) {
		try {
		this.Request = new XMLHttpRequest();
		}
		catch (e) {
			try {
			this.Request = new ActiveXObject('MSXML2.XMLHTTP');
			}
			catch (e) {
				try {
				this.Request = new ActiveXObject('Microsoft.XMLHTTP');
				}
				catch (e) {
				return false;
				}
			}
		}
	}

	if(this.Request.overrideMimeType ){
		if (this.ResponseFormat.toLowerCase()=="text")this.Request.overrideMimeType('text/html');
		if (this.ResponseFormat.toLowerCase()=="xml")this.Request.overrideMimeType('text/xml');
	}
	return this.Request;
}


this.decode=function(obj){
	if(typeof obj=="object"){
		var urlstr="";
		for (var k in obj){
			if(typeof obj[k]=="object"){
				for(var i=0;i<obj[k].length;i++){
					urlstr+=k+"="+encodeURIComponent(obj[k][i])+"&";
				}
			}else{
				urlstr+=k+"="+encodeURIComponent(obj[k])+"&";
			}	
		}
		if(urlstr.length>0)urlstr=urlstr.substr(0,urlstr.length-1);
	}else urlstr=obj;
	return urlstr;
}

this.encode=function(str){
	if(typeof obj=="string"){
		var obj={};
		var objArr=str.split("&");
		for (var i=0;i<objArr.length;i++){
			var kv=objArr[i].split("=");
			if (kv.length==2)obj[kv[0]]=decodeURIComponent(kv[1]);
		}
	}else obj=str;
	return obj;
}

this.Send=function(){
    if (!this.init()) {
		alert('Could not create XMLHttpRequest object.');
		return;
    }
    var self = this;
	self.ResponseFormat=self.ResponseFormat.toLowerCase();
    this.Request.onreadystatechange=function() {
		var resp = null;
		if (self.Request.readyState == 4) {

			switch (self.ResponseFormat.toLowerCase()) {
				case "text":
				resp = self.Request.responseText;
				break;
				
				case "xml":
				resp = self.Request.responseXML;
				break;
				
				case "request":
				resp = self.Request;
				break;
				
				case "object":
				resp = self.encode(self.Request.responseText);
				break;
				
				case "json":
				if(self.Request.responseText==""){
					resp=null;
				}else{
					if(window.JSON)
						resp = JSON.parse(self.Request.responseText);
					else resp = eval('('+self.Request.responseText+')');
				}
				break;
			}
			if (!self.Async){
				if (self.ResponseHandler!=null)self.ResponseHandler(resp,self.ObjectState);
				if(self.Request.onload)self.Request.onload=null;
			}
			if (self.Request.status >= 200 && self.Request.status <= 304) {
				if (self.ResponseHandler!=null)self.ResponseHandler(resp,self.ObjectState);
				if(self.Request.onload)self.Request.onload=null;
			}else {
				if(self.ErrorHandler!=null)self.ErrorHandler(resp);
					else alert("Problem while connecting to the server, Please refresh the page and try again");
			}
		}
    };

	if(this.Request.onload)this.Request.onload=this.Request.onreadystatechange
    if (this.Method.toLowerCase()=="get"){
		this.URL +="?"+this.decode(this.Data)
		this.Data=null
    }else{ 
		this.Data=this.decode(this.Data)     
    }
    this.Request.open(this.Method, this.URL, this.Async);
    if (this.Method.toLowerCase()=="post")this.Request.setRequestHeader("Content-type", "application/x-www-form-urlencoded")
    this.Request.send(this.Data);
};


// abort
this.abort = function() {
	if (this.Request) {
		this.Request.onreadystatechange = function() {};
		this.Request.abort();
		this.Request = null;
	};
};

};