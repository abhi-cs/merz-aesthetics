<?php get_header(); ?>

	<div id="wrap">
		<main id="main" class="site-main" role="main">

			<section id="page">
				<div id="featured-image-container">
					<div class="featured-image">
						<?php the_post_thumbnail('1080');?>
					</div>
					<div id="featured-info-container">
						<h1 class="title" href="<?php the_permalink();?>"><?php the_title();?></h1>
						<p class="header-excerpt"><?php echo get_field('header_excerpt'); ?> <p>
					</div>
				</div> <!--thumb-large-container-->
				

				<?php if(is_page( 'for-healthcare-professionals' )){?>

					<?php 
						if(isset($_SESSION['username']) ){
							echo 'tester';
							header('Location: http://localhost:8888/merz-aesthetics/for-healthcare-professionals/portal/');
					} ?>
					
					<div class="spacer"></div>
					<div class="spacer"></div>

					<div id="sign-in-box">
						<div class="col-container">
							
							<?php if (strpos($_SERVER['REQUEST_URI'], 'error') !== false) {
								echo "<p class='error'>There was an error. Please try again.</p>";
							}?>
							
							<?php if (strpos($_SERVER['REQUEST_URI'], 'loggedout') !== false) {
								echo "<p class='error'>You have been logged out.</p>";
							}?>
							
							<?php if (strpos($_SERVER['REQUEST_URI'], 'registered') !== false) {
								echo "<p class='error'>Your request for registration has been submitted. We will notify you when your request has been verified.</p>";
							}?>

							<div class="col-left">
								<?php if(get_bloginfo('language') == "en-CA"){
									include 'doctor-registration-form-eng.php';
								} else if(get_bloginfo('language') == "fr-CA"){
									include 'doctor-registration-form-fr.php';
								}?>
								<?php the_content(); ?>
								<div class="spacer"></div>
								<a id="healthcare-professional-register-now" class="round green"><?php pll_e('Register Now')?></a>

					
							</div>

							<div class="col-right">
								<?php if(get_bloginfo('language') == "en-CA"){
									include 'doctor-sign-in-form-eng.php';
								} else if(get_bloginfo('language') == "fr-CA"){
									include 'doctor-sign-in-form-fr.php';
								}?>
							</div>
							<div class="clear"></div>
						</div>
					</div>
					
					<div class="spacer"></div>
					<div class="spacer"></div>

				<?php } elseif(is_page( 'find-a-doctor' )){ ?>
			
					<?php include 'products-array.php' ?>

				<?php } elseif(is_page( 'cya-guarantee' )){ ?>
					
					<div class="spacer"></div>
					<div class="spacer"></div>

					<div id="sign-in-box">
						<div class="col-container">
							
							<?php if (strpos($_SERVER['REQUEST_URI'], 'error') !== false) {
								echo "<p class='error'>There was an error. Please try again.</p>";
							}?>
							
							<?php if (strpos($_SERVER['REQUEST_URI'], 'registered') !== false) {
								echo "<p class='error'>Your Registration for the Merz CYA Guarantee has been completed.</p>";
							}?>

							<?php if(get_bloginfo('language') == "en-CA"){
								include 'cya-registration-form-eng.php';
							} else if(get_bloginfo('language') == "fr-CA"){
								include 'cya-registration-form-fr.php';
							}?>
							<?php the_content(); ?>
							<div class="spacer"></div>
							<a id="cya-register-now" class="round green"><?php pll_e('Register Now')?></a>

							<div class="clear"></div>
						</div>
					</div>
					
					<div class="spacer"></div>
					<div class="spacer"></div>
					

				<?php } else { ?>
					<div class="spacer"></div>

					<?php the_content(); ?>

					<?php include 'mailing-list-signup.php' ?>
					
					<?php include 'products-array.php' ?>

					<div class="spacer"></div>
				<?php }?>
			</section>

		</main><!-- #main -->
	</div><!-- .wrap -->

<?php get_footer(); ?>
