	<form method="post" id="doctor-registration-form" action="http://localhost:8888/merz-aesthetics/wp-content/themes/merzaesthetics/scripts/register-doctor.php">
		<h2><?pll_e('Register Now')?></h2>
		<input class="full-width-input" type="text" name="title" id="doctor-register-title" placeholder="Title"/> 
		<input class="full-width-input" type="text" name="first-name" id="doctor-register-first-name" placeholder="First Name*" required/> 
		<input class="full-width-input" type="text" name="middle-name" id="doctor-register-middle-name" placeholder="Middle Name"/> 
		<input class="full-width-input" type="text" name="last-name" id="doctor-register-last-name" placeholder="Last Name*" required/> 
		<input class="full-width-input" type="text" name="suffix" id="doctor-register-suffix" placeholder="Suffix"/> 
		<input class="full-width-input" type="email" name="email" id="doctor-register-email" placeholder="Your E-mail*" required/> 


		<input class="full-width-input" type="text" name="doctor-register-username" id="doctor-register-username" placeholder="Username*" required/> 
		<input class="full-width-input" type="password" name="doctor-register-password" id="doctor-register-password" placeholder="Password*" required/> 

		<input class="full-width-input" type="text" name="clinic-name" id="clinic-name" placeholder="Clinic Name*" required/> 



		<input class="full-width-input" type="text" name="address" id="address" placeholder="Street Address*" required/> 
		<input class="half-width-input" type="text" name="city" id="city" placeholder="City*" required/> 
		<input class="half-width-input" type="text" name="province" id="province" placeholder="Province*" required/> 
		<input class="half-width-input" type="text" name="postalcode" id="postal-code" placeholder="Postal Code*" required/> 
		<input class="half-width-input" type="text" name="country" id="country" placeholder="Country*" required/> 
		<input class="half-width-input" type="text" name="phone" id="phone" placeholder="Phone Number"/> 
		<input class="half-width-input" type="text" name="fax" id="fax" placeholder="Fax Number"/> 
		<input class="full-width-input" type="text" name="website-address" id="website-address" placeholder="Website"/> 
		<input type="checkbox" name="accepts-web-inquiries" id="accepts-web-inquiries"/> <span>We accept web inquiries.</span>
		<input class="full-width-input" type="email" name="contact-email" id="contact-email" placeholder="E-mail for Web Inquiries"/> 
		<br/>

		<input type="checkbox" name="permission-for-radiesse" id="permission-for-radiesse"/> <span>I give permission for this information to appear on the Radiesse Website</span><br/>
		<input type="checkbox" name="permission-for-xeomin" id="permission-for-xeomin"/> <span>I give permission for this information to appear on the Xeomin Cosmetic Website</span><br/>
		<input type="checkbox" name="permission-for-belotero" id="permission-for-belotero"/> <span>I give permission for this information to appear on the Belotero Website</span><br/>
		<input type="checkbox" name="permission-for-cellfina" id="permission-for-cellfina"/> <span>I give permission for this information to appear on the Cellfina Website</span><br/>
		<input type="checkbox" name="permission-for-ultherapy" id="permission-for-ultherapy"/> <span>I give permission for this information to appear on the Ultherapy Website</span><br/>

		<p>Whereas, I wish to have my name, company name (if applicable) and professional contact information to Merz Pharma Canada Ltd. (“Merz Pharma Canada”) for publication on its web site(s) or other media (the “Sites”) (the “Information Publication”).</p>

		<p>Therefore, on my behalf and on behalf of any company identified below through which or n association with which I pro- vide my professional services and in respect of which I have he authority to bind (all references to “I” or “me” herein also include any such company):</p>
		<input class="full-width-input" type="text" name="individual-names" id="individual-names" placeholder="Individual Names"/> 
		<input class="full-width-input" type="text" name="company-name" id="company-name" placeholder="Company Name"/> 
		<input class="full-width-input" type="text" name="business-phone" id="business-phone" placeholder="Business Phone"/> 
		<input class="full-width-input" type="text" name="address2" id="address2" placeholder="Address:" readonly/> 
		<input class="half-width-input" type="text" name="signed-at" id=signed-at value="<?php echo date("m-d-y");?>" readonly/>
		<input class="half-width-input" type="text" name="signed-by" id="signed-by" placeholder="Your Signature"/>  
		<input class="half-width-input" type="text" name="witnessed-by" id="witnessed-by" placeholder="Witness Signature"/>  
		<div class="spacer"></div>
		<p class="small-text">*Mandatory fields.</p>
		<input id="submit-register" class="round green" type="submit" value="Submit"/>
		<div class="spacer"></div>
		<p class="small-text">By registering for the Merz Doctor Portal you agree to the <a href="../PractitionerLicenseAgreement.pdf">Practitioner License Agreement.</a></p>
		<p class="small-text">You will also be registered to a mailing list which we will use to contact you about major updates to the support material provided within.</p>
	</form>