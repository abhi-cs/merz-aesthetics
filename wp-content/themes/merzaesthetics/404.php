<?php get_header(); ?>

	<div id="wrap">
		<main id="main" class="site-main" role="main">

			<section id="page">
				<div id="featured-image-container">
					<div class="featured-image">
						<?php the_post_thumbnail('1080');?>
					</div>
					<div id="featured-info-container">
						<h1 class="title">404 - Page not Found</h1>
						<p class="header-excerpt">The page you're looking for is not available.<p>
					</div>
				</div> <!--thumb-large-container-->
				
					<div class="spacer"></div>
			
			<?php include 'latest-articles.php' ?>

			</section>

			<?php include 'mailing-list-signup.php' ?>
			
			<?php include 'products-array.php' ?>

		</main><!-- #main -->
	</div><!-- .wrap -->

<?php get_footer(); ?>
