<?php get_header(); ?>

	<?php 
		if( get_field('spot_colour') ): ?>
			<style>
				a.find-a-doc:before{
					color:<?php echo get_field('spot_colour')?>;
				}
			</style>
	<?php endif;?>

	<div id="mederma-wrap">
		<main id="main" class="site-main" role="main">

			<section id="solutions" class="<?php the_title();?>">
					<div id="featured-image-container" class="mederma">
						<div class="mederma featured-image">
							<?php the_field('header_image');?>
						</div>
						<div id="featured-info-container">

							<h2 class="header-tagline"><?php the_title(); ?> <p>

							<p class="header-excerpt"><?php echo get_field('header_excerpt'); ?> <p>

						</div>
					</div> <!--featured-image-container-->
					
					<div class="spacer"></div>
					<div id="the-content">
						<?php
							if ( function_exists('yoast_breadcrumb') ) {
								yoast_breadcrumb('
								<p id="breadcrumbs">','</p>');
							}
						?>
						<div class="wp-block-columns has-2-columns">
							<div class="wp-block-column">
								<p class="full-width"><?php the_post_thumbnail(); ?></p>
							</div>
							<div class="wp-block-column">
								<?php the_content(); ?>
							</div>
						</div>

						<?php if(get_field('directions_for_use')):?>
							<div class="spacer"></div>
							<h2 class="mederma-purple-text">Directions for use</h2>
							<p><?php echo the_field('directions_for_use')?></p>
						<?php endif; ?>

						<?php if(get_field('faqs')):?>
							<div class="spacer"></div>
							<h2 class="mederma-purple-text">FAQs</h2>
							<p><?php echo the_field('faqs')?></p>
						<?php endif; ?>

						<?php if(get_field('success_stories')):?>
							<div class="spacer"></div>
							<h2 class="mederma-purple-text">Success Stories</h2>
							<p><?php echo the_field('success_stories')?></p>
						<?php endif; ?>

					</div>

					<div class="spacer"></div>
			</section>

			<?php include 'latest-articles.php' ?>

		</main><!-- #main -->
	</div><!-- .wrap -->


<?php get_footer(); ?>
