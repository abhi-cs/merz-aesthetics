var map;
var markers = [];
var infoWindow;
var locationSelect;

function initMap() {
	var canada = {lat: 55.652591, lng: -95.2372269};
	map = new google.maps.Map(document.getElementById('map'), {
		center: canada,
		zoom: 4.45,
		mapTypeId: 'roadmap',
		mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU}
	});
	infoWindow = new google.maps.InfoWindow();

	searchButton = document.getElementById("search-button").onclick = searchLocations;

	locationSelect = document.getElementById("location-list");
	locationSelect.onchange = function() {
		var markerNum = locationSelect.options[locationSelect.selectedIndex].value;
		if (markerNum != "none"){
							google.maps.event.trigger(markers[markerNum], 'click');
		}
	};
		$("#map").css("width","100%").css("height","600px");
}

function searchLocations() {
	var address = document.getElementById("address-input").value;
	var geocoder = new google.maps.Geocoder();
	geocoder.geocode({address: address}, function(results, status) {
		if (status == google.maps.GeocoderStatus.OK) {
			searchLocationsNear(results[0].geometry.location);
		} else {
			alert(address + ' not found');
		}
	});
}

function clearLocations() {
	infoWindow.close();
	for (var i = 0; i < markers.length; i++) {
		markers[i].setMap(null);
	}
	markers.length = 0;

	$("#locations-ul").html("");
}


function searchLocationsNear(center) {
	var address = document.getElementById("address-input").value;
	
	dataLayer.push({
		'event': 'Search_Click'
	});

	clearLocations();

	var radius = '50';
	if($("#search-button").closest('section').attr('id') == "cellfina-find-a-doctor"){
		var searchUrl = 'https://api.merzcanada.com/cellfina/clinics?q=' + address;

		downloadUrl(searchUrl, function(data) {

			var jsondata = JSON.parse (data);
			var markerNodes = jsondata.clinics;
			var bounds = new google.maps.LatLngBounds();

			if (markerNodes.length == 0){
					var latlng = new google.maps.LatLng(55.652591,-95.2372269)
				bounds.extend(latlng);

			google.maps.event.addListenerOnce(map, 'bounds_changed', function(event) {
				if ( this.getZoom() ){ 
					this.setZoom(4); 
				}});
			}
			else{
				for (var i = 0; i < markerNodes.length; i++){
					var id = markerNodes[i].id;
					var name = markerNodes[i].business_name;
					if (markerNodes[i].address_2){
						var suite = markerNodes[i].address_2
					} else{
						var suite = ""
					}
					var address = markerNodes[i].address_1 + ", "  + suite + "<br/>" + markerNodes[i].city + " " + markerNodes[i].province + " " + markerNodes[i].postal_code;
					var phone = markerNodes[i].phone;
					var distance = markerNodes[i].distance
					var website = markerNodes[i].website;
					var latlng = new google.maps.LatLng(
							parseFloat(markerNodes[i].latitude),
							parseFloat(markerNodes[i].longitude)
						)
					var identifier = i;

					createMarker(latlng, name, address, phone, website, identifier);
					createList(latlng, name, address, phone, website, identifier);
					bounds.extend(latlng);
				}
			}
			if (bounds.getNorthEast().equals(bounds.getSouthWest())) {
					var extendPoint = new google.maps.LatLng(bounds.getNorthEast().lat() + 0.01, bounds.getNorthEast().lng() + 0.01);
					bounds.extend(extendPoint);
			}
			map.fitBounds(bounds);
			locationSelect.style.visibility = "visible";
			locationSelect.onchange = function() {
				var markerNum = locationSelect.options[locationSelect.selectedIndex].value;
				google.maps.event.trigger(markers[markerNum], 'click');
			};
		});
	}
	else if($("#search-button").closest('section').attr('id') == "ultherapy-find-a-doctor"){
		var searchUrl = 'https://api.merzcanada.com/ulthera/clinics?q=' + address + "&radius=50";

		downloadUrl(searchUrl, function(data) {

			var jsondata = JSON.parse (data);
			var markerNodes = jsondata.data;
			var bounds = new google.maps.LatLngBounds();

			if (markerNodes.length == 0){
					var latlng = new google.maps.LatLng(55.652591,-95.2372269)
				bounds.extend(latlng);

			google.maps.event.addListenerOnce(map, 'bounds_changed', function(event) {
				if ( this.getZoom() ){ 
					this.setZoom(4); 
				}});
			}
			else{
				for (var i = 0; i < markerNodes.length; i++){
					var id = markerNodes[i].id;
					var name = markerNodes[i].locator.name;
					var address = markerNodes[i].locator.street_address + ", " + "<br/>" + markerNodes[i].locator.city + " " + markerNodes[i].locator.state + " " + markerNodes[i].locator.postal_code;
					var phone = markerNodes[i].locator.phone;
					var distance = markerNodes[i].locator.distance
					var website = markerNodes[i].locator.website;
					var latlng = new google.maps.LatLng(
							parseFloat(markerNodes[i].locator.lat),
							parseFloat(markerNodes[i].locator.lng)
						)
					var identifier = i;
					if(markerNodes[i].add_to_locator == 1){
						createMarker(latlng, name, address, phone, website, identifier);
						createList(latlng, name, address, phone, website, identifier);
						bounds.extend(latlng);
					}
				}
			}
			if (bounds.getNorthEast().equals(bounds.getSouthWest())) {
					var extendPoint = new google.maps.LatLng(bounds.getNorthEast().lat() + 0.01, bounds.getNorthEast().lng() + 0.01);
					bounds.extend(extendPoint);
			}
			map.fitBounds(bounds);
			locationSelect.style.visibility = "visible";
			locationSelect.onchange = function() {
				var markerNum = locationSelect.options[locationSelect.selectedIndex].value;
				google.maps.event.trigger(markers[markerNum], 'click');
			};
		});
	}

	$("#map-container").addClass("searched");
}

function createMarker(latlng, name, address, phone, website, identifier) {
	var windowloc = window.location.toString();
	if (windowloc.substr(-3) == "fr/"){
		var html = "<span class=doctor-name>" + name + "</span><br/><p class=locator-text>" + address + "</p><br/><p class=locator-text>" + phone + "</p><br/> <a href='" + website +"' target='_blank' class=doctor-name>Visitez le site</a>";
	}
	else{
		var html = "<span class=doctor-name>" + name + "</span><br/><p class=locator-text>" + address + "</p><br/><p class=locator-text>" + phone + "</p><br/> <a href='" + website +"' target='_blank' class=doctor-name>Visit Website</a>";
	}

	var marker = new google.maps.Marker({
		map: map,
		position: latlng,
		id: identifier
	});
	google.maps.event.addListener(marker, 'click', function() {
		infoWindow.setContent(html);
		infoWindow.open(map, marker);
	});
	markers.push(marker);
}
function createList(latlng, name, address, phone, website, identifier){
	var windowloc = window.location.toString();
	if (windowloc.substr(-3) == "fr/"){
		var html = "<li><span class=doctor-name>" + name + "</span><br/><p class=locator-text>" + address + "</p><br/><p class=locator-text>" + phone + "</p><br/> <a href='" + website +"' target='_blank' class=doctor-name>Visitez le site</a></li><br/><hr/>";
	}
	else{
		var html = "<li id='"+ identifier + "'><span class=doctor-name>" + name + "</span><br/><p class=locator-text>" + address + "</p><br/><p class=locator-text>" + phone + "</p><br/> <a href='" + website +"' target='_blank' class=doctor-name>Visit Website</a></li><br/><hr/>";
	}
	$("#locations-ul").append(html);
}


function downloadUrl(url, callback) {
	var request = window.ActiveXObject ?
	new ActiveXObject('Microsoft.XMLHTTP') :
		new XMLHttpRequest;

		request.onreadystatechange = function() {
			if (request.readyState == 4) {
				request.onreadystatechange = doNothing;
				callback(request.responseText, request.status);
			}
		};

	request.open('GET', url, true);
	request.send(null);
}

function parseXml(str) {
	if (window.ActiveXObject) {
		var doc = new ActiveXObject('Microsoft.XMLDOM');
			doc.loadXML(str);
			return doc;
	} else if (window.DOMParser) {
		return (new DOMParser).parseFromString(str, 'text/xml');
	}
}

function doNothing() {}

function ready(){
	$('body').addClass("ready")
}

$(window).scroll(function() {
	var height = $(window).scrollTop();

	if(height  > 100) {
		$("#header-fixed, #menu-icon").addClass('down')
	}
	else{
		$("#header-fixed, #menu-icon").removeClass('down')
	}
});


$(document).ready(function(){

	var registrationForm = 0;

	ready();

	if(window.location.href.indexOf("#find-a-doctor") > -1) {
		$('html, body').animate({
			scrollTop: $("#address-bar").offset().top - 60
		}, 2000);
	}
	$(".find-a-doc").click(function(e){
		e.preventDefault();
		$('html, body').animate({
			scrollTop: $("#address-bar").offset().top - 60
		}, 2000);
	})
	
	$("#menu-icon").click(function(){
		$("#menu-icon, #side-menu, #dark-overlay").toggleClass("open");
	})
	
	function link_is_external(link_element) {
		return (link_element.host !== window.location.host);
	}

	$('a').click(function(e){
		if($(this).hasClass("no-transition") || link_is_external(this) || $(this).hasClass("cli-plugin-button")){

		}
		else if($(this).hasClass("video-pop")) {
			$iframesrc = $(this).attr("data-url");
			e.preventDefault();
			$("#dark-overlay, #videooverlay").toggleClass("open");
			$("#videocontainer").html($iframesrc);
		}
		else{
			var $href = $(this).attr('href');
			e.preventDefault();
			$("#menu-icon, #side-menu, #dark-overlay").removeClass("open");
			//$("html, body").animate({ scrollTop: "0px" }, 1000, function(){
				$("body").css('opacity','0')
				window.location.href = $href ;
			//});
		}
	});

	$("#locations-ul").on("hover", "li", function(){
		google.maps.event.trigger(markers[$(this).attr('id')], 'click');
	});

	$("#healthcare-professional-register-now").click(function(){
		$("#doctor-registration-form").slideToggle();
		registrationForm +=1;
		if(registrationForm % 2 === 0){
			$("#healthcare-professional-register-now").html("Register Now")
		}
		else{
			$("#healthcare-professional-register-now").html("Close")
		}
	})

	$("#cya-register-now").click(function(){
		$("#cya-registration-form").slideToggle();
		registrationForm +=1;
		if(registrationForm % 2 === 0){
			$("#cya-register-now").html("Register Now")
		}
		else{
			$("#cya-register-now").html("Close")
		}
	})

	$("#edit-details").click(function(){
		$("#doctor-edit-details-form").slideToggle();
		$("#account-information").slideToggle();
		registrationForm +=1;
		if(registrationForm % 2 === 0){
			$("#edit-details").html("Edit Details")
		}
		else{
			$("#edit-details").html("Cancel")
		}
	})

	$("#address-input").on('input', function() {
		var query = $("#addressInput").val();
		$("#search-button").attr('data-location', query);
	});

	$(".sim-vid").click(function(){
		this.paused ? this.play() : this.pause();
		$(this).attr("controls", "true");
	});

	$(".results").click(function(e){
		e.preventDefault();
		$("#dark-overlay").addClass("open");
		$("#ba-overlay").addClass("active");
		$("#ba-overlay #slider #before-after-images li").removeClass("active");
		$("#ba-overlay #slider #before-after-images li:nth-child(1)").addClass("active");
	})
	$("#dark-overlay, .modal-btn .close").click(function(){
		$("#dark-overlay").removeClass("open");
		$(".modal-btn").removeClass("active");
		$("#map-container").removeClass("searched");
	});

});