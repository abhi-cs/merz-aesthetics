<?php 

$firstName = strip_tags($_POST['first-name']);
$middleName = strip_tags($_POST['middle-name']);
$lastName = strip_tags($_POST['last-name']);
$username = strip_tags($_POST['doctor-register-username']);
$password = strip_tags($_POST['doctor-register-password']);
$email = strip_tags($_POST['email']);
$companyName = strip_tags($_POST['company-name']);
$accountCode = strip_tags($_POST['account-code']);
$title = strip_tags($_POST['title']);
$suffix = strip_tags($_POST['suffix']);
$address = strip_tags($_POST['address']);
$city = strip_tags($_POST['city']);
$province = strip_tags($_POST['province']);
$postalcode = strip_tags($_POST['postalcode']);
$country = strip_tags($_POST['country']);
$clinicName = strip_tags($_POST['clinic-name']);
$combinedAddress = $address . ',' . $city . ',' . $province . ',' . $postalcode;
$phone = strip_tags($_POST['phone']);
$fax = strip_tags($_POST['fax']);
$websiteAddress = strip_tags($_POST['website-address']);
$hoursOfOperation = strip_tags($_POST['hours-of-operation']);
$permissionForRadiesse = strip_tags($_POST['permission-for-radiesse']);
$permissionForXeomin = strip_tags($_POST['permission-for-xeomin']);
$permissionForBelotero = strip_tags($_POST['permission-for-belotero']);
$permissionForCellfina = strip_tags($_POST['permission-for-cellfina']);
$permissionForUltherapy = strip_tags($_POST['permission-for-ultherapy']);
$acceptsWebInquiries = strip_tags($_POST['accepts-web-inquiries']);
$contactEmail = strip_tags($_POST['contact-email']);
$individualNames = strip_tags($_POST['individual-names']);
$companyNames = strip_tags($_POST['company-name']);
$businessPhone = strip_tags($_POST['business-phone']);
$signedAt = strip_tags($_POST['signed-at']);
$signedBy = strip_tags($_POST['signed-by']);
$witnessedBy = strip_tags($_POST['witnessed-by']);

$apikey = '572b6c0f5d12a9d6e0d39d93d7df7597-us19';
$auth = base64_encode( 'user:'.$apikey );

$data = array(
	'apikey'        => $apikey,
	'email_address' => $email,
	'status'        => 'subscribed',
	'merge_fields'  => [
		'FNAME' => $firstName,
		'LNAME' => $lastName,
		'ADDRESS' => array(
				'addr1' => $clinicName,
				'addr2' => $address,
				'city' => $city,
				'state' => $province,
				'zip' => $postalcode,
				'country' => $country
				)
	]
);

$json_data = json_encode($data);

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, 'https://us19.api.mailchimp.com/3.0/lists/b9a15bb67f/members/');
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: Basic '.$auth));
curl_setopt($ch, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_TIMEOUT, 10);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);

$result = curl_exec($ch);


$curl = curl_init();

curl_setopt_array($curl, array(
	CURLOPT_URL => "https://cxapi.merzcanada.com/api/users/?include=account,merz-contacts,hcp-website-forms",
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 30,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "POST",
	CURLOPT_POSTFIELDS => "{\n\t\"data\": {\n\t\t\"id\": null,\n\t\t\"type\": \"users\",\n\t\t\"attributes\": {\n\t\t\t\"username\": \"$username\",\n\t\t\t\"password\": \"$password\",\n\t\t\t\"email\": \"$email\",\n\t\t\t\"first_name\": \"$firstName\",\n\t\t\t\"last_name\": \"$lastName\",\n\t\t\t\"middle_name\": \"$middleName\",\n\t\t\t\"personal_title\": \"$title\",\n\t\t\t\"suffix\": \"$suffix\",\n\t\t\t\"company_name\": \"$companyName\",\n\t\t\t\"account_code\": \"$accountCode\",\n\t\t\t\"hcp_website_form_attributes\": {\n\t\t\t\t\"clinic_name\":\"$clinicName\",\n\t\t\t\t\"healthcare_professional\":\"true\",\n\t\t\t\t\"address\":\"$combinedAddress\",\n\t\t\t\t\"phone\":\"$phone\",\n\t\t\t\t\"fax\":\"$fax\",\n\t\t\t\t\"website_address\":\"$websiteAddress\",\n\t\t\t\t\"hours_of_operation\":\"$hoursOfOperation\",\n\t\t\t\t\"permission_for_radiesse\":\"permissionForRadiesse\",\n\t\t\t\t\"permission_for_xeomin\":\"$permissionForXeomin\",\n\t\t\t\t\"permission_for_belotero\":\"$permissionForBelotero\",\n\t\t\t\t\"permission_for_cellfina\":\"$permissionForCellfina\",\n\t\t\t\t\"permission_for_ultherapy\":\"$permissionForUltherapy\",\n\t\t\t\t\"accepts_web_inquiries\":\"$acceptsWebInquiries\",\n\t\t\t\t\"contact_email\":\"$contactEmail\",\n\t\t\t\t\"individual_names\":\"$individualNames\",\n\t\t\t\t\"company_names\":\"$companyNames\",\n\t\t\t\t\"business_phone\":\"$businessPhone\",\n\t\t\t\t\"clinic_address\":\"$address\",\n\t\t\t\t\"clinic_city\":\"$city\",\n\t\t\t\t\"clinic_province\":\"$province\",\n\t\t\t\t\"clinic_postal_code\":\"$postalcode\",\n\t\t\t\t\"signed_at\":\"$signedAt\",\n\t\t\t\t\"signed_by\":\"signedBy\",\n\t\t\t\t\"witnessed_by\":\"$witnessedBy\"}\n\t\t},\n\t\t\"relationships\": {\n\t\t}\n\t}\n}\n",
	CURLOPT_HTTPHEADER => array(
		"Accept: application/cxapi.merzcanada.com.v1",
		"Content-Type: application/json",
		"MERZ-CXP-CLIENT: 62963aca-3d83-4176-8f7a-114f2e76bae0"
	),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
	header("Location:http://localhost:8888/merz-aesthetics/for-healthcare-professionals/?state=error");
} else {

	header("Location:http://localhost:8888/merz-aesthetics/for-healthcare-professionals/?state=registered");
}


?>