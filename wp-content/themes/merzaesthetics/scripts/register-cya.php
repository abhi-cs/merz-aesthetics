<?php 

$firstName = strip_tags($_POST['first-name']);
$middleName = strip_tags($_POST['middle-name']);
$lastName = strip_tags($_POST['last-name']);
$email = strip_tags($_POST['email']);
$address = strip_tags($_POST['address']);
$city = strip_tags($_POST['city']);
$province = strip_tags($_POST['province']);
$postalcode = strip_tags($_POST['postalcode']);
$country = strip_tags($_POST['country']);
$treatmentdate = strip_tags($_POST['treatment-date']);
$clinicname = strip_tags($_POST['clinic-name']);


$apikey = '572b6c0f5d12a9d6e0d39d93d7df7597-us19';
$auth = base64_encode( 'user:'.$apikey );

$data = array(
	'apikey'        => $apikey,
	'email_address' => $email,
	'status'        => 'subscribed',
	'merge_fields'  => [
		'FNAME' => $firstName,
		'LNAME' => $lastName,
		'TREATMENTD' => $treatmentdate,
		'CLINICNAME' => $clinicname,
		'ADDRESS' => array(
				'addr1' => $address,
				'city' => $city,
				'state' => $province,
				'zip' => $postalcode,
				'country' => $country
				)
	]
);

$json_data = json_encode($data);

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, 'https://us19.api.mailchimp.com/3.0/lists/91695615c9/members/');
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: Basic '.$auth));
curl_setopt($ch, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_TIMEOUT, 10);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);

$result = curl_exec($ch);

if ($err) {
	header("Location:http://localhost:8888/merz-aesthetics/cya-guarantee/?state=error");
} else {
	$recipient = $email;

	$subject = $orderid . " - CYA Guarantee";

	$headers = "From: " . $email . "\r\n";
	$headers .= "Reply-To: " . $email . "\r\n";
	$headers .= "MIME-Version: 1.0\r\n";
	$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

	$message="Thank you for registering for the Merz Aesthetics CYA Guarantee.";

	$message = '<html><body>';
	$message .= 'Thank you for registering your procedure at' . $clinicname . 'for the Merz Aesthetics CYA Guarantee.';
	$message .= "</body></html>";

	$message = "
	<head>
	<meta charset='UTF-8'>
	<meta name='viewport' content='width=device-width, initial-scale=1.0'>

	<style type='text/css'>
	body {
		margin: 0;
	}
	body, table, td, p, a, li, blockquote {
		-webkit-text-size-adjust: none!important;
		font-family: Arial, 'Helvetica', sans-serif;
		font-style: normal;
		font-weight: 400;
	}
	button{
		width:90%;
	}
	@media screen and (max-width:600px) {
	/*styling for objects with screen size less than 600px; */
	body, table, td, p, a, li, blockquote {
		-webkit-text-size-adjust: none!important;
		font-family: Arial, 'Helvetica', sans-serif;
	}
	table {
		/* All tables are 100% width */
		width: 100%;
	}
	.footer {
		/* Footer has 2 columns each of 48% width */
		height: auto !important;
		max-width: 48% !important;
		width: 48% !important;
	}
	table.responsiveImage {
		/* Container for images in catalog */
		height: auto !important;
		max-width: 30% !important;
		width: 30% !important;
	}
	table.responsiveContent {
		/* Content that accompanies the content in the catalog */
		height: auto !important;
		max-width: 66% !important;
		width: 66% !important;
	}
	.top {
		/* Each Columnar table in the header */
		height: auto !important;
		max-width: 48% !important;
		width: 48% !important;
	}
	.catalog {
		margin-left: 0%!important;
	}

	}
	@media screen and (max-width:480px) {
	/*styling for objects with screen size less than 480px; */
	body, table, td, p, a, li, blockquote {
		-webkit-text-size-adjust: none!important;
		font-family: Arial, 'Helvetica', sans-serif;
	}
	table {
		/* All tables are 100% width */
		width: 100% !important;
		border-style: none !important;
	}
	.footer {
		/* Each footer column in this case should occupy 96% width  and 4% is allowed for email client padding*/
		height: auto !important;
		max-width: 96% !important;
		width: 96% !important;
	}
	.table.responsiveImage {
		/* Container for each image now specifying full width */
		height: auto !important;
		max-width: 96% !important;
		width: 96% !important;
	}
	.table.responsiveContent {
		/* Content in catalog  occupying full width of cell */
		height: auto !important;
		max-width: 96% !important;
		width: 96% !important;
	}
	.top {
		/* Header columns occupying full width */
		height: auto !important;
		max-width: 100% !important;
		width: 100% !important;
	}
	.catalog {
		margin-left: 0%!important;
	}
	button{
		width:90%!important;
	}
	}
	</style>
	</head>
	<body yahoo='yahoo' style='background:#f2f2f2;'>
	<table width='100%'  cellspacing='0' cellpadding='0'>
	  <tbody>
	    <tr>
	      <td><table width='600'  align='center' cellpadding='0' cellspacing='0'>
	          <!-- Main Wrapper Table with initial width set to 60opx -->
	          <tbody>
	            <tr> 
	              <!-- HTML Spacer row -->
	              <td style='font-size: 0; line-height: 0;' height='20'><table width='96%' align='left'  cellpadding='0' cellspacing='0'>
	                  <tr>
	                    <td style='font-size: 0; line-height: 0;' height='20'>&nbsp;</td>
	                  </tr>
	                </table></td>
	            </tr>
	            <tr> 
	              <!-- Introduction area -->
	              <td><table width='96%'  align='left' cellpadding='0' cellspacing='0' style='-webkit-box-shadow:#ccc 0px 10px 20px;	-moz-box-shadow:#ccc 0px 10px 20px; box-shadow:#ccc 0px 10px 20px;'>
	                  <tr> 
	                    <!-- row container for TITLE/EMAIL THEME -->
	                    <td align='center' style='background:#242424; padding:20px 0px 20px 0px; font-size: 32px; font-weight: 300; line-height: 1.5em; color: #FFF; text-transform:uppercase; font-family: Arial, 'Helvetica', sans-serif, sans-serif;'> CYA Guarantee<br/></td>
	                  </tr>
	                  <tr> </tr>
	                  <tr> 
	                    <!-- Row container for Intro/ Description -->
	                    <td align='left' style='background: #FFF; font-size: 14px; padding: 10px; font-style: normal; font-weight: 100; color: #666; line-height: 1.8; text-align: justify; padding: 10px 20px 0px 20px; font-family: Arial, 'Helvetica', sans-serif, sans-serif; border: #808080 4px solid;'> <p>Hi " . $firstName . " " . $middleName . " " . $lastName . ",</p><p>Thank you for registering your procedure at " . $clinicname . "on" . $treatmentdate . " with the Merz CYA Guarantee. Please keep this email for your records, and feel free to contact us with any questions you might have.</p><br/><p>Sincerely,<br/>The Merz Aesthetics Team</p><br/><img src='http://localhost:8888/merz-aesthetics/wp-content/uploads/2018/08/merz-aesthetics-logo-01.svg' alt='Merz Aesthetics' style='width:200px; height:auto;'/><br/><br/><br/></td>
	                  </tr>
	                </table></td>
	            </tr>
	            <tr> 
	              <!-- HTML Spacer row -->
	              <td style='font-size: 0; line-height: 0;' height='10'><table width='96%' align='left'  cellpadding='0' cellspacing='0'>
	                  <tr>
	                    <td style='font-size: 0; line-height: 0;' height='20'>&nbsp;</td>
	                  </tr>
	                </table></td>
	            </tr>
	          </tbody>
	        </table></td>
	    </tr>
	  </tbody>
	</table>
	</body>
	";

	mail($recipient, $subject, $message, $headers) or die("Error!");



	header("Location:http://localhost:8888/merz-aesthetics/cya-guarantee/?state=registered");

}


?>