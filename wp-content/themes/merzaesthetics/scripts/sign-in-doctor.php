<?php 

$username = strip_tags($_POST['doctor-sign-in-username']);
$password = strip_tags($_POST['doctor-sign-in-password']);



$curl = curl_init();

curl_setopt_array($curl, array(
	CURLOPT_URL => "https://cxapi.merzcanada.com/api/sessions/?include=account,merz-contacts",
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 30,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "POST",
	CURLOPT_POSTFIELDS => "{\n\t\"data\": {\n\t\t\"id\": null,\n\t\t\"type\": \"authorizations\",\n\t\t\"attributes\": {\n\t\t\t\"username\": \"$username\",\n\t\t\t\"password\": \"$password\"\n\t\t},\n\t\t\"relationships\": {\n\t\t}\n\t}\n}",
	CURLOPT_HTTPHEADER => array(
	"Accept: application/cxapi.merzcanada.com.v1",
	"Content-Type: application/json",
	"MERZ-CXP-CLIENT: 62963aca-3d83-4176-8f7a-114f2e76bae0"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
	echo "cURL Error #:" . $err;
} else {
	echo $response;
	die();
}

?>