<?php

$curl = curl_init();

curl_setopt_array($curl, array(
	CURLOPT_URL => "https://cxapi.merzcanada.com/api/sessions/",
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 30,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "DELETE",
	CURLOPT_POSTFIELDS => "",
	CURLOPT_HTTPHEADER => array(
		"Accept: application/cxapi.merzcanada.com.v1",
		"Content-Type: application/json",
		"MERZ-CXP-CLIENT: 62963aca-3d83-4176-8f7a-114f2e76bae0"
	),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
	echo $response;
	session_unset();
	session_destroy();
}


	header('Location:http://localhost:8888/merz-aesthetics/for-healthcare-professionals/?state=loggedout');

?>