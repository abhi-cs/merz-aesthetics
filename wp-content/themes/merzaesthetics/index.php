<?php get_header(); ?>

	<div id="wrap">
		<main id="main" class="site-main" role="main">

		<?php include 'latest-articles.php' ?>

		<?php include 'mailing-list-signup.php' ?>

		<?php include 'mission-statement.php' ?>

		<?php include 'products-array.php' ?>

		</main><!-- #main -->
	</div><!-- .wrap -->

<?php get_footer(); ?>
