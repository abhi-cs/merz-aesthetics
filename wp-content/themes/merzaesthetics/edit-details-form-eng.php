	<form method="post" id="doctor-edit-details-form" action="http://localhost:8888/merz-aesthetics/wp-content/themes/merzaesthetics/scripts/edit-details-doctor.php">
		<h2><?pll_e('Edit Details')?></h2>
		<input type="hidden" name="id" value="<?php echo $userInfo->data->id ?>">
		<input type="hidden" name="old-email" value="<?php echo $userInfo->data->attributes->email ?>">
		<input class="full-width-input" type="text" name="title" id="doctor-register-title" placeholder="Title" value="<?php echo
						$userInfo->data->attributes->personalTitle ?>"/> 
		<input class="full-width-input" type="text" name="first-name" id="doctor-register-first-name" placeholder="First Name*" value="<?php echo $userInfo->data->attributes->firstName ?>" required/> 
		<input class="full-width-input" type="text" name="middle-name" id="doctor-register-middle-name" placeholder="Middle Name" value="<?php echo $userInfo->data->attributes->middleName ?>"/> 
		<input class="full-width-input" type="text" name="last-name" id="doctor-register-last-name" placeholder="Last Name*" value="<?php echo $userInfo->data->attributes->lastName ?>" required/> 
		<input class="full-width-input" type="text" name="suffix" id="doctor-register-suffix" placeholder="Suffix" value="<?php echo $userInfo->data->attributes->suffix ?>"/> 
		<input class="full-width-input" type="email" name="email" id="doctor-register-email" placeholder="Your E-mail*" value="<?php echo $userInfo->data->attributes->email ?>" required/> 

		<input class="full-width-input" type="text" name="company-name" id="company-name" placeholder="Company Name*" value="<?php echo $userInfo->data->attributes->companyName ?>"  required/> 


		<input type="hidden" class="full-width-input" type="text" name="address" id="address" placeholder="Full Address*" value="<?php echo $userInfo->included[2]->attributes->clinicAddress ?>" required/> 
		<input type="hidden" class="half-width-input" type="text" name="city" id="city" placeholder="City*"  value="<?php echo $userInfo->included[2]->attributes->clinicCity ?>" required/> 
		<input type="hidden" class="half-width-input" type="text" name="province" id="province" placeholder="Province*"  value="<?php echo $userInfo->included[2]->attributes->clinicProvince ?>" required/> 
		<input type="hidden" class="half-width-input" type="text" name="postalcode" id="postal-code" placeholder="Postal Code*"  value="<?php echo $userInfo->included[2]->attributes->clinicPostalCode ?>" required/> 
		<input type="hidden" class="half-width-input" type="text" name="country" id="country" placeholder="Country*"  value="Canada" required/> 


		<input class="half-width-input" type="text" name="phone" id="phone" placeholder="Phone Number" value="<?php echo $userInfo->included[2]->attributes->phone ?>"/> 
		<input class="half-width-input" type="text" name="fax" id="fax" placeholder="Fax Number" value="<?php echo $userInfo->included[2]->attributes->fax ?>"/> 
		<input type="hidden" class="full-width-input" type="text" name="website-address" id="website-address" placeholder="Website" value="<?php echo $userInfo->included[2]->attributes->websiteAddress ?>"/> 

		<input class="full-width-input" type="text" name="account-code" id="account-code" placeholder="Account Code"  value="<?php echo $userInfo->data->attributes->accountCode ?>"/> 
		<div class="spacer"></div>
		<p class="small-text">*Mandatory fields.</p>
		<input id="submit-register" class="round green" type="submit" value="Submit"/>
		<div class="spacer"></div>
	</form>