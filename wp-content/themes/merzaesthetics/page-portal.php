
<?php get_header(); ?>

<?php 

if($_SESSION['username'] ){
	$username = $_SESSION['username'];
	$password = $_SESSION['password'];
} else{
	$username = strip_tags($_POST['doctor-sign-in-username']);
	$password = strip_tags($_POST['doctor-sign-in-password']);
}




$curl = curl_init();

curl_setopt_array($curl, array(
	CURLOPT_URL => "https://cxapi.merzcanada.com/api/sessions/?include=account,merz-contacts,hcp-website-forms",
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_HEADER => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 30,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "POST",
	CURLOPT_POSTFIELDS => "{\n\t\"data\": {\n\t\t\"id\": null,\n\t\t\"type\": \"authorizations\",\n\t\t\"attributes\": {\n\t\t\t\"username\": \"$username\",\n\t\t\t\"password\": \"$password\"\n\t\t},\n\t\t\"relationships\": {\n\t\t}\n\t}\n}",
	CURLOPT_HTTPHEADER => array(
	"Accept: application/cxapi.merzcanada.com.v1",
	"Content-Type: application/json",
	"MERZ-CXP-CLIENT: 62963aca-3d83-4176-8f7a-114f2e76bae0"
  ),
));

$response = curl_exec($curl);
$responseUser = stristr($response, '{');
$err = curl_error($curl);

curl_close($curl);

$headers = [];
$data = explode("\n",$response);
$headers['status'] = $data[0];
array_shift($data);

foreach($data as $part){
    $middle=explode(":",$part);
    $headers[trim($middle[0])] = trim($middle[1]);
}

$token = $headers['Authorization'];

if ($err) {
	echo "cURL Error #:" . $err;
} else { 
	$userInfo = json_decode($responseUser);
	if (!$userInfo->data->id){
		header('Location: '.get_permalink(53).'?state=error');
	}
	else{
		session_start();
		$_SESSION['username'] = $username;
		$_SESSION['password'] = $password;
		$_SESSION['auth'] = $token;
	}
}
?>



<script type="text/javascript" src="http://localhost:8888/merz-aesthetics/wp-content/portal/ajax.js';?>"></script>
<script type="text/javascript" src="http://localhost:8888/merz-aesthetics/wp-content/portal/browser.js';?>"></script>
<script type="text/javascript">
//console.log(userInfo);

function init(){
browser({
	contentsDisplay:document.getElementById("dvContents"),
	refreshButton:document.getElementById("btnrefresh"),
	pathDisplay:document.getElementById("pPathDisplay"),
	filter:document.getElementById("txtFilter"),
	openFolderOnSelect:true,
	onSelect:function(item,params){
		if(item.type=="folder"){

		}
		else{
    		window.location.href = "http://localhost:8888/merz-aesthetics/wp-content/portal/dir/" + item.path;
		}
	},
	currentPath:"dir"
	});
}
</script>


	<div id="wrap">
		<main id="main" class="site-main" role="main">

			<section id="portal">
				<div id="featured-image-container">
					<div class="featured-image">
						<?php the_post_thumbnail('1080');?>
					</div>
					<div id="featured-info-container">
						<h1 class="title" href="<?php the_permalink();?>">
						<?php echo
						$userInfo->data->attributes->personalTitle . " " .
						$userInfo->data->attributes->firstName . " " .
						$userInfo->data->attributes->middleName . " " .
						$userInfo->data->attributes->lastName . " " .
						$userInfo->data->attributes->suffix;?></h1>
						<p class="header-excerpt"><?php echo get_field('header_excerpt'); ?> <p>
					</div>
				</div> <!--thumb-large-container-->
					
					<div class="spacer"></div>

					
						<div class="col-container">
							
							<div class="col-left">
							
								<div class="browser">
									<p id="pPathDisplay" class="pPathDisplay">Loading...</p>
									<div id="dvContents" class="dvContents">&nbsp;</div>
								</div>
					
							</div>

							<div class="col-right">
								<p><?pll_e('Need help? Contact us:')?></p>
								<h2><?php echo $userInfo->included[1]->attributes->firstName . " " . $userInfo->included[1]->attributes->lastName;?></h2>
								<p><a href="mailto:<?php echo $userInfo->included[1]->attributes->email ?>"><?php echo $userInfo->included[1]->attributes->email ?></a> <br/>
								<?php echo $userInfo->included[1]->attributes->title ?></p>
							</div>

							<div class="col-right">
								<section id="edit-details-section">
									<?php if(get_bloginfo('language') == "en-CA"){
										include 'edit-details-form-eng.php';
									} else if(get_bloginfo('language') == "fr-CA"){
										include 'edit-details-form-fr.php';
									}?>
								</section>

								<section id="account-information">
									<p><b><?pll_e('Your Account Information')?></b></p>
									<p><?pll_e('Name:')?> <?php echo
							$userInfo->data->attributes->personalTitle . " " .
							$userInfo->data->attributes->firstName . " " .
							$userInfo->data->attributes->middleName . " " .
							$userInfo->data->attributes->lastName . " " .
							$userInfo->data->attributes->suffix;?> </p>
									<p><?pll_e('E-mail:')?> <a href="mailto:<?php echo $userInfo->data->attributes->email ?>"><?php echo $userInfo->data->attributes->email ?></a> </p>
									<p><?pll_e('Company Name:')?> <?php echo $userInfo->data->attributes->companyName ?> </p>
									<p><?pll_e('Address:')?> <?php echo $userInfo->included[2]->attributes->address ?> </p>
									<p><?pll_e('Account Code:')?> <?php echo $userInfo->data->attributes->accountCode ?> </p>
									<br/>
								</section>
								<a class="round green" href="<?php echo get_permalink(53).'?state=loggedout' ?>"><?pll_e('Log out')?></a>
								<a id="edit-details" class="round green"><?pll_e('Edit Details')?></a>
								<div class="spacer"></div>
							</div>
							<div class="clear"></div>
						</div>

					<div class="spacer"></div>

			</section>

		</main><!-- #main -->
	</div><!-- .wrap -->

<?php get_footer(); ?>
