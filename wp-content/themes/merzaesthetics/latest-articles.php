		
		<section id="latest-articles">

			<?php if (is_front_page()){?>
			
				<?php
					$args = array(
						'post_type' 		=> 'articles',
						'posts_per_page' 	=> 1,
					);

					$postslist = new WP_Query($args);
					if($postslist->have_posts() ) :
						while ($postslist->have_posts() ) : $postslist->the_post();
				?>

					<div id="featured-image-container">
						<div class="featured-image">
							<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('1080');?></a>
						</div>
						<a class="title" href="<?php the_permalink();?>"><?php the_title();?></a>
						<p class="category"><?php echo get_the_term_list(get_the_ID(), 'category', '', ', ', ''); ?> <p>
					</div> <!--featured-image-container-->
				<?php endwhile; endif;?>
			
			<section id="three-articles">
				<?php
					$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
					$args = array(
						'post_type' 		=> 'articles',
						'posts_per_page' 	=> 3,
						'offset'			=> 1
					);

					$postslist = new WP_Query($args);
					if($postslist->have_posts() ) :
						while ($postslist->have_posts() ) : $postslist->the_post();
				?>

					<div class="thumb-large-container">
						<div class="thumb-large">
							<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('720 Cropped');?></a>
						</div>
						<a class="title" href="<?php the_permalink();?>"><?php the_title();?></a>
						<p class="category"><?php echo get_the_term_list(get_the_ID(), 'category', '', ', ', ''); ?> </p>
					</div> <!--thumb-large-container-->
				<?php endwhile; endif;?>
			</section>

			<?php } else{?>
			
			<section id="three-articles">
				<?php
					$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
					$args = array(
						'post_type' 		=> 'articles',
						'posts_per_page' 	=> 3,
					);

					$postslist = new WP_Query($args);
					if($postslist->have_posts() ) :
						while ($postslist->have_posts() ) : $postslist->the_post();
				?>

					<div class="thumb-large-container">
						<div class="thumb-large">
							<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('720 Cropped');?></a>
						</div>
						<a class="title" href="<?php the_permalink();?>"><?php the_title();?></a>
						<p class="category"><?php echo get_the_term_list(get_the_ID(), 'category', '', ', ', ''); ?> </p>
					</div> <!--thumb-large-container-->
				<?php endwhile; endif;?>
			</section>
			<?php } ?>

		</section>
		<div class="clear"></div>