<?php

	function get_cat_slug($cat_id) {
		$cat_id = (int) $cat_id;
		$category = &get_category($cat_id);
		return $category->slug;
	}

	
	//add featured image support
	add_theme_support( 'post-thumbnails' ); 
	
	//header logo
	add_theme_support( 'custom-logo' );

	add_filter('tiny_mce_before_init', 'customize_tinymce');

	function customize_tinymce($in) {
		$in['paste_preprocess'] = "function(pl,o){ o.content = o.content.replace(/p class=\"p[0-9]+\"/g,'p'); o.content = o.content.replace(/span class=\"s[0-9]+\"/g,'span'); }";
		return $in;
	}

	//add custom menus
	add_action( 'init', 'my_custom_menus' );
	function my_custom_menus() {
		register_nav_menus(
			array(
				'top-menu' => __( 'Top Menu' ),
				'footer-links' => __( 'Footer Links' ),
				'portal-link' => __( 'Portal Link' ),
				'logged-in-portal-link' => __( 'Logged In Portal Link' ),
				'mederma-categories' => __( 'Mederma Categories' )
			)
		);
	}
	//remove anchor from read more
	function remove_more_jump_link($link) { 
		$offset = strpos($link, '#more-');
		if ($offset) { $end = strpos($link, '"',$offset); }
		if ($end) { $link = substr_replace($link, '', $offset, $end-$offset); }
		return $link;
	}
	add_filter('the_content_more_link', 'remove_more_jump_link');

	//add pagetheme support
	function custom_theme_setup() {
		add_theme_support( $feature, $arguments );
	}
	add_action( 'after_setup_theme', 'custom_theme_setup' );
	
	//add image sizes
	add_image_size( '1080', 1920, 1080 );
	add_image_size( '720', 1280, 720 );
	add_image_size( '720 Cropped', 1280, 720, array( 'center', 'center')  );
	add_image_size( '600', 600, 600 );
	add_image_size( '540', 960, 540 );
	add_image_size( '360', 640, 360 );
	add_image_size( '640 Square', 640, 640, array( 'center', 'center') );
	add_image_size( '270', 480, 270 );

	//add categories to pages
	function add_taxonomies_to_pages() {
		register_taxonomy_for_object_type( 'post_tag', 'page' );
		register_taxonomy_for_object_type( 'category', 'page' );
	}
	
	add_action( 'init', 'add_taxonomies_to_pages' );

	if ( ! is_admin() ) {
		add_action( 'pre_get_posts', 'category_and_tag_archives' ); 
	}
	function category_and_tag_archives( $wp_query ) {
		$my_post_array = array('post','page');
		if ( $wp_query->get( 'category_name' ) || $wp_query->get( 'cat' ) )
			$wp_query->set( 'post_type', $my_post_array );
	 
		if ( $wp_query->get( 'tag' ) )
			$wp_query->set( 'post_type', $my_post_array );
	}

	//custom styles in WYSIWYG editor
	function wysiwyg_btns($buttons) {
		array_unshift($buttons, 'styleselect');
		return $buttons;
	}
	add_filter('mce_buttons_2', 'wysiwyg_btns');

	function editor_styles() {
		add_editor_style( 'custom-editor-style.css' );
	}
	add_action( 'init', 'editor_styles' );

	//no paragraph tag for image
	function filter_ptags_on_images($content){
		return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
	}
	add_filter('the_content', 'filter_ptags_on_images');

	//svg mime
	function cc_mime_types($mimes) {
		$mimes['svg'] = 'image/svg+xml';
		return $mimes;
	}
	add_filter('upload_mimes', 'cc_mime_types');


	//polylang strings
//	add_action('init', function() {
//		pll_register_string('Find a Doctor', 'Find a Doctor', false);
//		pll_register_string('Read More', 'Read More', false);
//	});

	//add jquery support
	if (!is_admin()) add_action("wp_enqueue_scripts", "my_scripts_enqueue", 11);
		function my_scripts_enqueue() {
			wp_enqueue_style( 'style', get_stylesheet_uri() );
			wp_deregister_script('jquery');
			wp_register_script('jquery', "http" . ($_SERVER['SERVER_PORT'] == 443 ? "s" : "") . "://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js", false, null);
			wp_enqueue_script('jquery');
			wp_register_script('scripts', get_template_directory_uri() . "/scripts/scripts.js", null, true);
			wp_enqueue_script('scripts');
		}
?>