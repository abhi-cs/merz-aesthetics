	<form method="post" id="cya-registration-form" action="http://localhost:8888/merz-aesthetics/wp-content/themes/merzaesthetics/scripts/register-cya.php">
		<h2><?pll_e('Register Now')?></h2>
		<input class="full-width-input" type="text" name="first-name" id="doctor-register-first-name" placeholder="First Name*" required/> 
		<input class="full-width-input" type="text" name="middle-name" id="doctor-register-middle-name" placeholder="Middle Name"/> 
		<input class="full-width-input" type="text" name="last-name" id="doctor-register-last-name" placeholder="Last Name*" required/> 
		<input class="full-width-input" type="email" name="email" id="doctor-register-email" placeholder="Your E-mail*" required/> 

		<input class="half-width-input" type="text" name="treatment-date" id="treatment-date" placeholder="Treatment Date: MM / DD / YYYY" required/> 
		<input class="half-width-input" type="text" name="clinic-name" id="clinic-name" placeholder="Clinic Name*" required/> 

		<input class="full-width-input" type="text" name="address" id="address" placeholder="Full Address*" required/> 
		<input class="half-width-input" type="text" name="city" id="city" placeholder="City*" required/> 
		<input class="half-width-input" type="text" name="province" id="province" placeholder="Province*" required/> 
		<input class="half-width-input" type="text" name="postalcode" id="postal-code" placeholder="Postal Code*" required/> 
		<input class="half-width-input" type="text" name="country" id="country" placeholder="Country*" required/> 

		<p class="small-text">*Mandatory fields.</p>
		<input id="submit-register" class="round green" type="submit" value="Submit"/>
		<div class="clear"></div>
		<p class="small-text">By registering for the Merz CYA Guarantee you agree to the <a href="../__.pdf">Terms of Service.</a></p>
	</form>