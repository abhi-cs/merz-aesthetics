<?php get_header(); ?>		
		<section id="products">

			<?php
				$args = array(
					'post_type' 		=> 'solutions',
					'posts_per_page' 	=> 3
				);

				$postslist = new WP_Query($args);
				if($postslist->have_posts() ) :
					while ($postslist->have_posts() ) : $postslist->the_post();
			?>

				<div class="thumb-large-container">
					<div class="thumb-large">
						<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('1080');?></a>
					</div>

					<a class="read-more" href="<?php the_permalink();?>"><?php pll_e('Read More')?></a>
				</div> <!--thumb-large-container-->
			<?php endwhile; endif;?>

		</section>
<?php get_footer(); ?>