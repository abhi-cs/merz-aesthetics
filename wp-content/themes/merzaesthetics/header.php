<?php 
if(session_id() == ''){
	session_start();
} ?>


<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head  <?php language_attributes(); ?>>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NTC8CGP');</script>
<!-- End Google Tag Manager -->


	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<title>Merz Aesthetics</title>

	<link href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,700" rel="stylesheet">
	<?php wp_head(); ?>
</head>

<?php if(is_page(229)){ ?>

<body onload="init()" <?php body_class(); ?>>

<?php } else { ?>

<body <?php body_class(); ?>>

<?php } ?>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NTC8CGP"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->


<?php
if (strpos($_SERVER['REQUEST_URI'], 'loggedout') !== false) {
	session_unset();
	session_destroy();
}
?>

	<div id="dark-overlay"></div>

	<div id="menu-icon" class="mobile">	
		<span></span>
		<span></span>
		<span></span>
		<span></span>
	</div>	

	<div id="side-menu" class="mobile">
		<?php wp_nav_menu (array('theme_location' => 'footer-links','container_id' => 'side-nav', 'menu_class' => 'mobile'));?>
	</div>

	<header>
		<div id="header-fixed">
			<div id="logo">
				<?php the_custom_logo(); ?>
			</div>


			<?php if($_SESSION['username'] ){
				wp_nav_menu (array('theme_location' => 'logged-in-portal-link', 'menu_class' => 'logged-in-healthcare-pro-link'));
			} else{
				wp_nav_menu (array('theme_location' => 'portal-link', 'menu_class' => 'healthcare-pro-link'));
			}?>

			<?php wp_nav_menu (array('theme_location' => 'top-menu','container_id' => 'top-nav'));?>
		</div>	

		<div class="spacer"></div>

	<div id="header-push"></div>
		<?php if (is_front_page()){
			echo do_shortcode('[smartslider3 slider=2]');
		} ?>
	</header>

