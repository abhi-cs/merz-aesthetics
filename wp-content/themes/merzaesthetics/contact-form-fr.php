				<form method="post" id="contact-form">
					<h2><?pll_e('Contact')?></h2>
					<input class="full-width-input" type="text" name="name" id="name" placeholder="Your Name*" required/> 
					<input class="full-width-input" type="email" name="email" id="email" placeholder="Your E-mail*" required/> 
					<div class="select-container">
						<select class="full-width-input" id="option" name="option">
							<option value="" disabled selected required>Topic*</option>
							<option value="Offers">Offers</option>
							<option value="Other">Other</option>
							<option value="Availability">Product Availability Question</option>
							<option value="Information">Product Information</option>
						</select>
						<div class="arrow"></div>
					</div>
					<input class="full-width-input" type="url" name="empty" id="empty" placeholder="Leave this empty"/>
					<textarea form="contact-form" class="textarea full-width-input" type="textarea" name="what" id="what" wrap="on" cols="30" rows="5" placeholder="How can we help?"></textarea>
					<p class="small-text">*Mandatory fields.</p>
					<input id="submit" type="submit" value="Submit" data-contents="scripts/contact.php"/>
					<div class="clear"></div>
					<p class="small-text">Please note that if you submit information to Merz all materials submitted by electronic or physical communication will be deemed non confidential. Do not submit any information or other materials that you consider to be confidential or proprietary. While we have no intention to publicly disclose any details of your proposal, we may refer to select experts within or outside the Merz network for evaluation.</p>
				</form>