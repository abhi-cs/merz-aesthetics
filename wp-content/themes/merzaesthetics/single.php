<?php get_header(); ?>


	<div id="wrap">
		<main id="main" class="site-main" role="main">

			<section id="article">
				<div id="featured-image-container">
					<div class="featured-image">
						<?php the_post_thumbnail('1080');?>
					</div>
					<h1 class="title" href="<?php the_permalink();?>"><?php the_title();?></h1>
					<p class="category"><?php echo get_field('author'); ?> <p>
				</div> <!--thumb-large-container-->
				
				<div class="spacer"></div>
						
						<?php
							if ( function_exists('yoast_breadcrumb') ) {
								yoast_breadcrumb('
								<p id="breadcrumbs">','</p>');
							}
						?>

				<?php the_content(); ?>

				<div class="spacer"></div>
			</section>
			
			<h3><?php pll_e('What to read next')?></h3>
			
			<?php include 'latest-articles.php' ?>

		</main><!-- #main -->
	</div><!-- .wrap -->

<?php get_footer(); ?>
