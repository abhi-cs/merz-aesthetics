<?php get_header(); ?>


<?php if( is_single(121) ) {?>
    <div id="solutions-wrap">
			<main id="main" class="site-main" role="main">

				<section id="solutions" class="<?php the_title();?>">
					<div id="featured-image-container" class="mederma">
						<div class="mederma featured-image">
							<?php the_field('header_image');?>
						</div>
						<div id="featured-info-container">
							<?php $product_logo = get_field('product_logo');

								if( !empty($product_logo) ){

									// vars
									$url = $product_logo['url'];
									$title = $product_logo['title'];
									$alt = $product_logo['alt'];
									$caption = $product_logo['caption'];

									// thumbnail
									$size = 'thumbnail';
									$thumb = $product_logo['sizes'][ $size ];
								};
							?>

							<img class="product-logo" src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" />

							<h2 class="header-tagline"><?php echo get_field('tagline'); ?> <p>

							<p class="header-excerpt"><?php echo get_field('header_excerpt'); ?> <p>

							<a class="no-transition round find-a-doc" id="header-find-a-doctor" href="#find-a-doctor"> Find a store</a>
						</div>
					</div> <!--featured-image-container-->
					
						
						<div class="spacer"></div>
						<div id="the-content">
							<?php
								if ( function_exists('yoast_breadcrumb') ) {
									yoast_breadcrumb('
									<p id="breadcrumbs">','</p>');
								}
							?>
						<div class="spacer"></div>
							<?php wp_nav_menu (array('theme_location' => 'mederma-categories','container_id' => 'mederma-categories'));?>

							<div id="mederma-scar-care-tab" class="mederma-tabs">
								<div class="mederma-category-image">
									<h2 class="caption">
										<?php echo wp_get_attachment_caption(attachment_url_to_postid( get_page_by_title('Mederma Scar Care - Category Image', OBJECT, 'attachment')->guid));
										?>
									</h2>
									<img alt="Woman who uses Mederma Scar Care products" src="<?php echo get_page_by_title('Mederma Scar Care - Category Image', OBJECT, 'attachment')->guid; ?>" />
								</div>
								<?php
									$args = array(
										'taxonomy' 			=> 'category',
										'term' 				=> 'scar-care',
										'post_type' 		=> 'medermaproducts',
										'posts_per_page' 	=> -1,
										'orderby'			=> 'meta_value',
										'order'				=> 'ASC'
									);

									$postslist = new WP_Query($args);
									if($postslist->have_posts() ) :
										while ($postslist->have_posts() ) : $postslist->the_post();
								?>
									<div class="mederma-product-item">
										<div class="thumb-large-container mederma">
											<div class="thumb-large">
												<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('1080');?></a>
											</div>
										</div> <!--thumb-large-container-->

										<div class="product-details">
											<h2 class="mederma-product"><?php the_title();?></h2>
											<p><?php the_field('excerpt')?></p>
											<a class="mederma learn-more" href="<?php the_permalink();?>"> Learn More</a>
										</div>
										<div class="clear"></div>
										<hr/>
									</div>
								<div class="spacer"></div>
								<?php endwhile; endif?>

							</div>

						</div>

						<div class="spacer"></div>
				</section>

				<?php include 'latest-articles.php' ?>

			</main><!-- #main -->
		</div><!-- .wrap -->
<?php }else{?>









		<?php 
			if( get_field('spot_colour') ): ?>
				<style>
					a.find-a-doc:before, .close{
						color:<?php echo get_field('spot_colour')?>;
					}
				</style>
		<?php endif;?>


	<div id="ba-overlay" class="modal-btn">
		<div class="close"><i class="fas fa-times-circle"></i></div>
			<p class="small">Individual results may vary.</p>
			<div id="slider">
			 <?php echo the_field('before_after_gallery'); ?>
			</div>
	</div>

		<div id="solutions-wrap">
			<main id="main" class="site-main" role="main">

				<section id="solutions" class="<?php the_title();?>">
					<div id="featured-image-container">
						<div class="featured-image">
							<?php the_field('header_image');?>
						</div>
						<div id="featured-info-container">
							<?php $product_logo = get_field('product_logo');

								if( !empty($product_logo) ){

									// vars
									$url = $product_logo['url'];
									$title = $product_logo['title'];
									$alt = $product_logo['alt'];
									$caption = $product_logo['caption'];

									// thumbnail
									$size = 'thumbnail';
									$thumb = $product_logo['sizes'][ $size ];
								};
							?>

							<img class="product-logo" src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" />

							<h2 class="header-tagline"><?php echo get_field('tagline'); ?> <p>

							<p class="header-excerpt"><?php echo get_field('header_excerpt'); ?> <p>

							<a class="no-transition round find-a-doc" id="header-find-a-doctor" style="background-color:<?php echo get_field("spot_colour")?>;"> <?php pll_e('Find a doctor')?></a>
						</div>
					</div> <!--featured-image-container-->
					
						
						<div class="spacer"></div>
						<div id="the-content">
							<?php
								if ( function_exists('yoast_breadcrumb') ) {
									yoast_breadcrumb('
									<p id="breadcrumbs">','</p>');
								}
							?>
							<?php the_content(); ?>
						</div>

						<div class="spacer"></div>
				</section>

				<?php include 'latest-articles.php' ?>

			</main><!-- #main -->
		</div><!-- .wrap -->

	<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDEadP3DX7cMWi8C3Dcgoj0LB_GGfgdyC4&region=CA&callback=initMap">
			</script>

<?php }?>
<?php get_footer(); ?>
