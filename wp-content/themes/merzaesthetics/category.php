<?php get_header(); ?>

<?php
	$cat = get_category( get_query_var( 'cat' ) );
   	$cat_slug = $cat->slug;
?>

	<div id="wrap">
		<main id="main" class="site-main" role="main">
			<?php
				$args = array(
					'post_type' 		=> 'articles',
					'tax_query'			=> array(
											array(
												'taxonomy'	=> 'category',
												'field'		=> 'slug',
												'terms'		=> $cat_slug,
												),
											),
					'posts_per_page' 	=> 1,
				);

				$postslist = new WP_Query($args);
				if($postslist->have_posts() ) :
					while ($postslist->have_posts() ) : $postslist->the_post();
			?>

			<div id="featured-image-container">
				<div class="featured-image">
					<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('1080');?></a>
				</div>
				<a class="title" href="<?php the_permalink();?>"><?php the_title();?></a>
				<p class="category"><?php echo get_the_term_list(get_the_ID(), 'category', '', ', ', ''); ?> <p>
			</div> <!--thumb-large-container-->
			
			<?php endwhile; endif;?>

			<div class="spacer"></div>
		
		<?php

			$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
			$args = array(
				'post_type'			=> 'articles',
				'tax_query'			=> array(
											array(
												'taxonomy'	=> 'category',
												'field'		=> 'slug',
												'terms'		=> $cat_slug,
											),
										),
				'posts_per_page'	=> 10,
				'offset'			=> 1,
				'paged'				=> $paged,
				'order'				=> 'DESC'
			);

				$postslist = new WP_Query($args);
				if($postslist->have_posts() ) :
					while ($postslist->have_posts() ) : $postslist->the_post();
			?>

			<div class="thumb-large-container">
				<div class="thumb-large">
					<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('720 Cropped');?></a>
				</div>
				<a class="title" href="<?php the_permalink();?>"><?php the_title();?></a>
				<p class="category"><?php echo get_the_term_list(get_the_ID(), 'category', '', ', ', ''); ?></p>
			</div> <!--thumb-large-container-->
		<?php endwhile; endif;?>


		<?php include 'mission-statement.php' ?>

		</main><!-- #main -->
	</div><!-- .wrap -->

<?php get_footer(); ?>
