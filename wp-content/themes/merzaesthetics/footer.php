		
		<div class="clear"></div>

		<footer>
			<div class="col-container">
				<div class="col-left">

				<?php if(get_bloginfo('language') == "en-CA"){
					include 'contact-form-eng.php';
				} else if(get_bloginfo('language') == "fr-CA"){
					include 'contact-form-fr.php';
				}?>
					
				</div>
				<div class="col-right">
					<?php wp_nav_menu (array('theme_location' => 'footer-links','container_id' => 'footer-nav', 'menu_class' => 'footer-links', 'item_separator' => '|'));?>	
					<div id="social-links">
						<span class="fa-stack fa-4x">
							<i class="fa fa-circle fa-stack-2x icon-background"></i>
							<a href="http://www.facebook.com/____________/" target="_blank"><i class="fab fa-facebook-square fa-stack-1x" aria-hidden="true"></i></a>
						</span>
						<span class="fa-stack fa-4x">
							<i class="fa fa-circle fa-stack-2x icon-background"></i>
							<a href="http://www.youtube.com/____________/" target="_blank"><i class="fab fa-youtube fa-stack-1x" aria-hidden="true"></i></a>
						</span>
						<span class="fa-stack fa-4x">
							<i class="fa fa-circle fa-stack-2x icon-background"></i>
							<a href="http://www.instagram.com/____________/" target="_blank"><i class="fab fa-instagram fa-stack-1x" aria-hidden="true"></i></a>
						</span>
					</div>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque vitae interdum ex. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Quisque vitae interdum ex. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis.</p>
				</div>
			</div>
			<div class="clear"></div>
		</footer>
		<section id="footnote">

			<p><b><?php pll_e('Safety Information')?></b></p>
			<?php while( have_posts() ) : the_post() ?>
				<?php if( get_field('safety_information') ): ?>
					<p><?php the_field('safety_information');?></p>
				<?php endif; endwhile; ?>

			<img alt="Merz Pharma Canada Ltd. Logo" src="<?php echo get_stylesheet_directory_uri(); ?>/images/merz-logo.svg"/>

			<?php if(get_bloginfo('language') == "en-CA"){?>
			<p>Copyright ® <?php echo date('Y'); ?> Merz Pharma Canada Ltd. All rights reserved. MERZ AESTHETICS and the MERZ AESTHETICS logo are registered trademarks of Merz Pharma GmbH & Co. KGaA. Cellfina is a trademark of Ulthera ®, Inc.'</p>
			<?php } else if(get_bloginfo('language') == "fr-CA"){?>
			<p>FRENCH FRENCH FRENCHCopyright ® <?php echo date('Y'); ?> Merz Pharma Canada Ltd. All rights reserved. MERZ AESTHETICS and the MERZ AESTHETICS logo are registered trademarks of Merz Pharma GmbH & Co. KGaA. Cellfina is a trademark of Ulthera ®, Inc.'</p>
			<?php }?>
		</section>
	</body>
	<?php wp_footer()?>
	
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
	</html>