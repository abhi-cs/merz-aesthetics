<?php
/*
Plugin Name: Site Plugin - Post Types
Description: Adds Articles and Solutions as post types
*/

function create_posttype(){
    $articles_supports = array(
		'title',
		'editor',
		'excerpt',
		'author', 
		'thumbnail',
		'revisions',
		'custom-fields',
		'page-attributes'
	);
	$articles_labels = array(
		'name' 					=> __('Articles', 'Post Type General Name'),
		'singular_name'			=> __('Article', 'Post Type Singular Name'),
		'menu_name' 			=> __('Articles'),
		'all_items' 			=> __('All Articles'),
		'view_item'				=> __('View Article'),
		'add_new_iem'			=> __('Add New Article'),
		'add_new'				=> __('Add New Article'),
		'edit_item'				=> __('Edit Article'),
		'update_item'			=> __('Update Article'),
		'search_items'			=> __('Search Articles'),
		'not_found'				=> __('Not Found'),
		'not_found_in_trash'	=> __('Not found in Trash'),
		'archives'				=> __('Article Archives'),
		'featured_image'		=> __('Article Image'),
		'set_featured_image'	=> __('Set Image'),
		'remove_featured_image'	=> __('Remove Image'),
		'use_featured_image'	=> __('Use Image')
	);

	$articles_args = array (
 		'menu_icon' 			=> 'dashicons-list-view',
		'label'					=> __('Articles'),
		'description'			=> __('Articles'),
		'show_in_rest'			=> true,
		'taxonomies'			=> array( 'category', 'post_tag' ),
		'labels'				=> $articles_labels,
		'supports'				=> $articles_supports,
		'hierarchical'			=> false,
		'public'				=> true,
		'show_ui'				=> true,
		'show_in_menu'			=> true,
		'show_in_admin_bar'		=> true,
		'menu_position'			=> 4,
		'can_export'			=> true,
		'has_archive'			=> true,
		'exclude_from_search'	=> true,
		'publicly_queryable'	=> true,
		'show_in_nav_menus'		=> true,
		'capability_type'		=> 'post'

	);

	$solutions_supports = array(
		'title',
		'editor',
		'excerpt',
		'author', 
		'thumbnail',
		'revisions',
		'custom-fields',
		'page-attributes'
	);
	$solutions_labels = array(
		'name' 					=> __('Solutions', 'Post Type General Name'),
		'singular_name'			=> __('Solution', 'Post Type Singular Name'),
		'menu_name' 			=> __('Solutions'),
		'all_items' 			=> __('All Solutions'),
		'view_item'				=> __('View Solution'),
		'add_new_iem'			=> __('Add New Solution'),
		'add_new'				=> __('Add New Solution'),
		'edit_item'				=> __('Edit Solution'),
		'update_item'			=> __('Update Solution'),
		'search_items'			=> __('Search Solutions'),
		'not_found'				=> __('Not Found'),
		'not_found_in_trash'	=> __('Not found in Trash'),
		'archives'				=> __('Solution Archives'),
		'featured_image'		=> __('Solution Image'),
		'set_featured_image'	=> __('Set Image'),
		'remove_featured_image'	=> __('Remove Image'),
		'use_featured_image'	=> __('Use Image')
	);

	$solutions_args = array (
 		'menu_icon' 			=> 'dashicons-heart',
		'label'					=> __('Solutions'),
		'description'			=> __('Solutions'),
		'show_in_rest'			=> true,
		'taxonomies'			=> array( 'category', 'post_tag' ),
		'labels'				=> $solutions_labels,
		'supports'				=> $solutions_supports,
		'hierarchical'			=> false,
		'public'				=> true,
		'show_ui'				=> true,
		'show_in_menu'			=> true,
		'show_in_admin_bar'		=> true,
		'menu_position'			=> 5,
		'can_export'			=> true,
		'has_archive'			=> true,
		'exclude_from_search'	=> true,
		'publicly_queryable'	=> true,
		'show_in_nav_menus'		=> true,
		'capability_type'		=> 'post'

	);

    $medermaproducts_supports = array(
		'title',
		'editor',
		'excerpt',
		'author', 
		'thumbnail',
		'revisions',
		'custom-fields',
		'page-attributes'
	);
	$medermaproducts_labels = array(
		'name' 					=> __('Mederma Products', 'Post Type General Name'),
		'singular_name'			=> __('Mederma Product', 'Post Type Singular Name'),
		'menu_name' 			=> __('Mederma Products'),
		'all_items' 			=> __('All Mederma Products'),
		'view_item'				=> __('View Mederma Products'),
		'add_new_iem'			=> __('Add New Mederma Product'),
		'add_new'				=> __('Add New Mederma Product'),
		'edit_item'				=> __('Edit Mederma Product'),
		'update_item'			=> __('Update Mederma Product'),
		'search_items'			=> __('Search Mederma Products'),
		'not_found'				=> __('Not Found'),
		'not_found_in_trash'	=> __('Not found in Trash'),
		'archives'				=> __('Mederma Product Archives'),
		'featured_image'		=> __('Mederma Product Image'),
		'set_featured_image'	=> __('Set Image'),
		'remove_featured_image'	=> __('Remove Image'),
		'use_featured_image'	=> __('Use Image')
	);

	$medermaproducts_args = array (
 		'menu_icon' 			=> 'dashicons-archive',
		'label'					=> __('Mederma Products'),
		'description'			=> __('Mederma Products'),
		'show_in_rest'			=> true,
		'taxonomies'			=> array( 'category', 'post_tag' ),
		'labels'				=> $medermaproducts_labels,
		'supports'				=> $medermaproducts_supports,
		'hierarchical'			=> false,
		'public'				=> true,
		'show_ui'				=> true,
		'show_in_menu'			=> true,
		'show_in_admin_bar'		=> true,
		'menu_position'			=> 4,
		'can_export'			=> true,
		'has_archive'			=> true,
		'exclude_from_search'	=> true,
		'publicly_queryable'	=> true,
		'show_in_nav_menus'		=> true,
		'capability_type'		=> 'post'

	);

	register_post_type('Solutions', $solutions_args );
	register_post_type('Articles', $articles_args );
	register_post_type('Mederma Products', $medermaproducts_args );
	flush_rewrite_rules();
};
add_action ('init', 'create_posttype', 0);


/* Stop Adding Functions Below this Line */
?>